﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SessionData {

	private static GameDataSave GAME_DATA;

	public static bool LoadData() {
        var valid = false;

        var data = PlayerPrefs.GetString("data", "");
        if (data != "") {
	        var success = DESEncryption.TryDecrypt(data, out var original);
	        if (success) {
		        GAME_DATA = JsonUtility.FromJson<GameDataSave>(original);
		        GAME_DATA.LoadData();
		        valid = true;    
	        }
	        else {
		        GAME_DATA = new GameDataSave();
	        }
            
        } else {
            GAME_DATA = new GameDataSave();
        }

        return valid;
    }

    public static bool SaveData() {
        const bool valid = false;

        try {
            GAME_DATA.SaveData();
            var result = DESEncryption.Encrypt(JsonUtility.ToJson(SessionData.GAME_DATA));
            PlayerPrefs.SetString("data", result);
            //Debug.Log("Saved");
            PlayerPrefs.Save();
        } catch (Exception ex) {
            Debug.LogError(ex.ToString());
        }

        return valid;
    }

    public static GameDataSave Data {
        get {
			if (GAME_DATA == null)
                LoadData();
			return GAME_DATA;
        }
    }

	public static void ClearData()
	{
		GAME_DATA = new GameDataSave();
		SaveData();
	}

}


[Serializable]
public class GameDataSave {
	//Put attributes that you want to save during your game.

	public int xp;
	public int lifeHacker;
	public int lifeWizard;
	
	public float hpPlayerOne;
	public float manaPlayerOne;
	public float[] damagePowers;
	public float[] manaPowersCost;

	//Cosas de prueba

	public int cardBlackAmount;
	public int cardGoldenAmount;
	public int cardGreenAmount;
	public int cardVioletAmount;
	public int cardBlueAmount;

	public List<PlayerBattleSystem.PlayerPowers> currentPowers;
	public List<PlayerBattleSystem.PlayerPowers> powersInventoryList;

	//

	public int[] cardsAmount = { 0, 0 , 0 ,0, 0};
	public bool resettingSold;
	public bool healSold;
	public bool electroshockSold;
	public bool deleteSold;
	public bool controlzSold;
	public bool updateSold;
	public bool superHitSold;
	public bool canyonSold;
	public bool scannerSold;

	public GameDataSave()
	{
		
	}

	public void SaveData() {
		
    }

	public void LoadData()
	{
		
	}
}