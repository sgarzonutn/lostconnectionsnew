﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImage : MonoBehaviour
{
    private RawImage _background;
    
    [Header("References")]
    
    public Texture español;
    
    public Texture english;
    // Start is called before the first frame update
    void Start()
    {
        _background = GetComponent<RawImage>();
        if (PlayerPrefs.GetInt("LenguajeGuardado", 0) == 0)
        {
            _background.texture = english;
        }
        else
        {
            _background.texture = español;
        }
    }
    
}
