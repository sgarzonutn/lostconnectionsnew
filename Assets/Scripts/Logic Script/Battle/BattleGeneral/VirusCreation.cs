﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VirusCreation : MonoBehaviour
{
    public GameObject virusPrefab;

    private void Awake()
    {
        CreateEnemyVirus();
    }

    private void CreateEnemyVirus()
    {
        Transform virusPosition = GameObject.Find("ObjectsWorldScene/ObjectsScene/ObjectWorld/VirusInWorld").transform;

        switch (SceneManager.GetActiveScene().name)
        {
            case "SampleScene":
                {
                    for (int i = 0; i < virusPosition.childCount; i++)
                    {
                        if (PlayerPrefs.GetInt("DeathVirus" + i) == 0)
                        {
                            Transform virusObjects = Instantiate(virusPrefab, virusPosition.GetChild(i)).transform;
                            virusObjects.parent = virusPosition.GetChild(i);
                            virusObjects.name = "Enemy";
                        }
                    }

                    break;
                }
            case "BattleScene":
                {
                    Transform virusObjects = Instantiate(virusPrefab, virusPosition).transform;
                    virusObjects.parent = virusPosition;
                    virusObjects.name = "Enemy";
                    virusObjects.gameObject.AddComponent<EnemyBattleSystem>().enemyClass = EnemyBattleSystem.EnemyClass.virusNormal;

                    break;
                }
            case "BattleBoss":
                {
                    Transform virusObjects = Instantiate(virusPrefab, virusPosition).transform;
                    virusObjects.parent = virusPosition;
                    virusObjects.name = "Enemy";
                    virusObjects.gameObject.AddComponent<EnemyBattleSystem>().enemyClass = EnemyBattleSystem.EnemyClass.virusBoss;

                    break;
                }
        }
    }

}
