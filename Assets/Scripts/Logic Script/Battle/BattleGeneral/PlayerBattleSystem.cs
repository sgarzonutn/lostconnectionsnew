﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBattleSystem : MonoBehaviour
{
    public enum PlayerPowers //Los Prefabs en la carpeta de resources AttackFX tienen que estar en el mismo orden que estos enums
    {
        Bug, //Le saca 50 al virus en el nivel 1 pero le resta 60 de mana a uno
        Steal, //Le saca 10 al enemigo y le suma 30 en mana a uno mismo
        Pixel, //Le saca 20 al virus y le resta 30 de mana 
        Shock, //Le saca 20 al enemigo y resta 25 de mana
        Lighting, //Resta 50 al enemigo y resta 50 de mana
        Electricity, //Resta 50 al enemigo y resta 50 de mana

        ControlZ, //Poder exclusivo de charlie, 
                  //recupera la vida robada por el enemigo 
                  //(Revierte el ataque recibido pero resta 20 de mana)
        Reset, //Poder exclusivo de Charlie, reinicia toda la batalla desde cero, inclusive los estados tanto del equipo como el enemigo
        Delete, //Poder exclusivo de Charlie que suma tanto mana como daño que ocasiona el enemigo
        Heal, //Restaura la vida de todo el equipo incluidad la suya pero dejara inhabilitado para pelear momentaneamente
        Updating, //Restaura el mana de Atif pero no produce daño al enemigo
        Electroshock, //Poderoso ataque Atif que daña tanto al enemigo como la energia que quita

        Invalid
    }

    public enum PlayerClass
    {
        mage,
        hacker
    }

    public LifePlayer lifePlayer;
    public PlayerClass playerClass;
    public List<PlayerPowers> powers;
    public float hp;
    public float mana;

    public BattleMachine battleSystem;
    public ParticlesAttack particleSystem;

    public Animator animationsPlayer;

    //Attack values
    float powerValue = 0;
    PlayerPowers power;
    Transform enemy;

    void Awake()
    {
        lifePlayer = Resources.Load("Prefabs/Characters/LifePlayer") as LifePlayer;

        //La primera vez que se abre el juego usar las variables de LifePlayer
        hp = lifePlayer.hpPlayerOne;
        mana = lifePlayer.manaPlayerOne;

        //Despues de la primer batalla usar las variables de HP y MANA guardadas en playerprefs
        // hp = playerprefs.GetFloat("HP");
        // mana = playerprefs.GetFloat("MANA");

        playerClass = (PlayerClass)PlayerPrefs.GetInt("PlayerClass");
        animationsPlayer = GetComponent<Animator>();

        InitializePowers();
    }

    void Start()
    {
        battleSystem = GameObject.Find("GameManager").GetComponent<BattleMachine>();
        particleSystem = battleSystem.GetComponent<ParticlesAttack>();

        SetMana(mana); //Volverlo al Awake
    }

    private void InitializePowers()
    {
        powers = new List<PlayerPowers>();

        CardsCounterWorld cardsCounter = Resources.Load("Prefabs/Characters/Cards Counter World") as CardsCounterWorld;
        cardsCounter.Load();

        switch (playerClass)
        {
            case PlayerClass.hacker:
                {
                    powers.Add(PlayerPowers.Bug);
                    powers.Add(PlayerPowers.Steal);
                    powers.Add(PlayerPowers.Pixel);

                    cardsCounter.AddInventoryPower(PlayerPowers.Bug);
                    cardsCounter.AddInventoryPower(PlayerPowers.Steal);
                    cardsCounter.AddInventoryPower(PlayerPowers.Pixel);

                    if (cardsCounter.currentPowers.Count == 0)
                    {
                        cardsCounter.UpdateCurrentPower(0, PlayerPowers.Bug);
                        cardsCounter.UpdateCurrentPower(1, PlayerPowers.Steal);
                        cardsCounter.UpdateCurrentPower(2, PlayerPowers.Pixel);
                    }

                    break;
                }
            case PlayerClass.mage:
                {
                    powers.Add(PlayerPowers.Shock);
                    powers.Add(PlayerPowers.Lighting);
                    powers.Add(PlayerPowers.Electricity);

                    cardsCounter.AddInventoryPower(PlayerPowers.Shock);
                    cardsCounter.AddInventoryPower(PlayerPowers.Lighting);
                    cardsCounter.AddInventoryPower(PlayerPowers.Electricity);

                    if (cardsCounter.currentPowers.Count == 0)
                    {
                        cardsCounter.UpdateCurrentPower(0, PlayerPowers.Shock);
                        cardsCounter.UpdateCurrentPower(1, PlayerPowers.Lighting);
                        cardsCounter.UpdateCurrentPower(2, PlayerPowers.Electricity);
                    }

                    break;
                }
        }

        //Load players powers
        powers = cardsCounter.currentPowers;
    }

    public void Attack(PlayerPowers _power, Transform _enemy)
    {
        float manaValue = lifePlayer.GetPowerManaCost(_power);
        powerValue = lifePlayer.GetPowerDamage(_power);

        //Si manaValue es Negativo lo convertimos a Positivo para poder comparar con la mana actual del player.
        //Si manaValue es Positivo lo convertimos a Negativo para poder utilizarlo siempre aunque mana sea 0.
        float manaTemp = (manaValue * -1);

        if (mana >= manaTemp) //&& tarjeta
        {
            power = _power;
            enemy = _enemy;
            SetMana(manaValue);
            animationsPlayer.SetTrigger("Attack");
        }
        else
        {
            Debug.LogError("Mana insuficiente, no deberíamos poder seleccionar un ataque si no tenemos suficiente mana");
        }

        battleSystem.DisplayAttackOptions(null, false);
    }

    public void AttackAnimationEnd()
    {
        particleSystem.AttackPlayerPower(power, this, enemy);
        battleSystem.AttackPlayerEnd(powerValue);
    }

    public void AttackEnd()
    {
        battleSystem.AttackFXEnd();

        power = PlayerPowers.Invalid;
        enemy = null;
    }


    public List<PlayerPowers> GetPowers()
    {
        return powers;
    }

    public void SetDamage(float dmg)
    {
        animationsPlayer.SetTrigger("Damage");

        hp -= dmg;

        lifePlayer.hpPlayerOne = hp;

        if (hp <= 0 || lifePlayer.hpPlayerOne <= 0)
        {
            animationsPlayer.SetBool("Dead", true);
            battleSystem.ActivatePanel();
        }

    }

    public void SetMana(float manaValue)
    {
        mana += manaValue;

        if (mana > 100)
        {
            mana = 100;
        }
        else if (mana < 0)
        {
            mana = 0;
        }

        lifePlayer.manaPlayerOne = mana;
    }

    public void AddNewPower(PlayerPowers power)
    {
        powers.Add(power);
    }

    public void ReplacePower(PlayerPowers newPower, PlayerPowers powerRemoved)
    {
        for (int i = 0; i < powers.Count; i++)
        {
            if (newPower == powers[i])
            {
                powers[i] = newPower;
            }
        }
    }

    public float GetHP()
    {
        return hp;
    }

    public float GetMana()
    {
        return mana;
    }


}
