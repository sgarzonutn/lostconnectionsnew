﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleMachine : MonoBehaviour
{
    enum BattleState
    {
        Start,
        InProgress,
        End,
        None
    }

    enum Side
    {
        Players,
        Enemies
    }

    public PlayerBattleSystem player;
    public EnemyBattleSystem enemy;
    public int enemyNumber;

    public PanelUIBattle panelUIBattle;
    private Animator animationVirus;

    private BattleState currentState;
    private Side currentSide;

    private void Start()
    {
        player = GameObject.Find("ObjectsWorldScene/ObjectPlayers").transform.GetChild(0).GetComponent<PlayerBattleSystem>();
        enemy = GameObject.Find("ObjectsWorldScene/ObjectsScene/ObjectWorld/VirusInWorld").transform.GetChild(0).GetComponent<EnemyBattleSystem>();
        panelUIBattle = GameObject.Find("CanvasBattle/PnlBattleUI").GetComponent<PanelUIBattle>();

        enemyNumber = PlayerPrefs.GetInt("CurrentVirusNumber");
        animationVirus = enemy.GetComponent<Animator>();

        currentState = BattleState.None;
        currentSide = Side.Players;

        StartBattle();
    }

    public void StartBattle()
    {
        if (currentState == BattleState.None)
        {
            currentState = BattleState.Start;

            panelUIBattle.SetEnergyPlayer(player.GetHP());
            panelUIBattle.SetManaPlayer(player.GetMana());
            panelUIBattle.SetEnergyEnemy(enemy.GetHP());
            panelUIBattle.SetScapeButton(this);

            animationVirus.SetBool("SceneBattle", true);
            currentSide = Side.Players;

            Attack();
        }
    }

    public void AttackPlayerEnd(float dmg)
    {
        enemy.SetDamage(dmg);

        panelUIBattle.SetEnergyEnemy(enemy.GetHP());
        panelUIBattle.SetManaPlayer(player.GetMana());
    }

    public void AttackEnemyEnd(float dmg, PlayerBattleSystem player)
    {
        player.SetDamage(dmg);

        panelUIBattle.SetEnergyPlayer(player.GetHP());
    }

    public void AttackFXEnd()
    {
        NextTurn();
    }

    public void BattleEnd()
    {
        currentState = BattleState.End;

        CleanBattleComponents();
    }

    private void NextTurn()
    {
        if (IsEnemyDead() == true)
        {
            //TODO - Agregar panel rewards
            PlayerPrefs.SetInt("DeathVirus" + enemyNumber, 1);

            //Debug.Log("Virus " + enemyNumber + " muerto");

            BattleEnd();
            return;
        }
        else if (IsPlayerDead() == true)
        {
            //TODO - Agregar panel perdiste y sacar la llamada al metodo
            BattleEnd();
            return;
        }

        currentSide = currentSide == Side.Players ? currentSide = Side.Enemies : currentSide = Side.Players;

        Attack();
    }

    private void Attack()
    {
        if (currentState != BattleState.None)
        {
            if (currentSide == Side.Players)
            {
                DisplayAttackOptions(player, true);
            }
            else
            {
                enemy.Attack(player.transform);
            }

            currentState = BattleState.InProgress;
        }
    }

    public void DisplayAttackOptions(PlayerBattleSystem player, bool display)
    {
        panelUIBattle.transform.Find("ContainerBtn").gameObject.SetActive(display);

        if (display == false)
        {
            CleanAttackOptions();
        }

        if (player != null)
        {
            panelUIBattle.ShowAttackButtons(player, enemy.transform);
        }
    }

    private void CleanAttackOptions()
    {
        panelUIBattle.CleanAttackButtons();
    }

    private void CleanBattleComponents()
    {
        //ENEMIES
        if (enemy != null)
        {
            enemy.EnemyEndBattle();
            enemy = null;
            animationVirus.SetBool("SceneBattle", false);
        }

        CleanAttackOptions();
        DisplayAttackOptions(null, false);

        currentState = BattleState.None;

        if (SceneManager.GetActiveScene().name == "BattleBoss" && enemy == null)
        {
            panelUIBattle.ActivaImgWinGame();
        }

        if (SceneManager.GetActiveScene().name == "BattleScene")
        {
            SceneManager.LoadScene("SampleScene");
        }

    }

    private bool IsEnemyDead()
    {
        return (enemy == null) || (enemy.GetHP() <= 0);
    }

    private bool IsPlayerDead()
    {
        bool dead = false;

        if (player.GetHP() <= 0)
        {
            dead = true;
        }

        return dead;
    }

    public void ActivatePanel()
    {
        if (SceneManager.GetActiveScene().name == "BattleBoss")
            panelUIBattle.ActiveImgGameOver();
    }

    public void SetTextPanel()
    {
        panelUIBattle.SetTextInformation("No Mana");
    }

}
