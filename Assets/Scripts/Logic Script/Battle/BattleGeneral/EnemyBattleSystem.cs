﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBattleSystem : MonoBehaviour
{
    public enum EnemyPowers
    {
        Scan,
        Attack,
        Infect,
        ScanBoss,
        AttackBoss,
        InfectBoss,
        Invalid
    }

    public enum EnemyClass
    {
        virusNormal,
        virusBoss,
    }

    public List<EnemyPowers> powers;
    public EnemyClass enemyClass;
    public ParticlesAttack particleSystem;

    private Animator animationVirus;
    private BattleMachine battleSystem;

    public float hp;

    //Attack values
    float powerValue = 0;
    EnemyPowers power;
    Transform player;

    void Awake()
    {
        battleSystem = GameObject.Find("GameManager").GetComponent<BattleMachine>();
        particleSystem = battleSystem.GetComponent<ParticlesAttack>();
        animationVirus = GetComponent<Animator>();

        SetPowers();

        hp = 100;
    }

    void SetPowers()
    {
        powers = new List<EnemyPowers>();

        switch (enemyClass)
        {
            case EnemyClass.virusNormal:
                {
                    powers.Add(EnemyPowers.Scan);
                    powers.Add(EnemyPowers.Attack);
                    powers.Add(EnemyPowers.Infect);

                    break;
                }
            case EnemyClass.virusBoss:
                {
                    powers.Add(EnemyPowers.ScanBoss);
                    powers.Add(EnemyPowers.AttackBoss);
                    powers.Add(EnemyPowers.InfectBoss);

                    break;
                }
        }
    }


    public List<EnemyPowers> GetPowers()
    {
        return powers;
    }

    public void Attack(Transform _player)
    {
        switch (enemyClass)
        {
            case EnemyClass.virusNormal:
                {
                    power = (EnemyPowers)UnityEngine.Random.Range(0, 3);
                    break;
                }
            case EnemyClass.virusBoss:
                {
                    power = (EnemyPowers)UnityEngine.Random.Range(3, 6);
                    break;
                }
        }

        switch (power)
        {
            case EnemyPowers.Scan:
                {
                    powerValue = 30;
                    break;
                }
            case EnemyPowers.Attack:
                {
                    powerValue = 10;
                    break;
                }
            case EnemyPowers.Infect:
                {
                    powerValue = 20;
                    break;
                }
            case EnemyPowers.ScanBoss:
                {
                    powerValue = 30;
                    break;
                }
            case EnemyPowers.AttackBoss:
                {
                    powerValue = 30;
                    break;
                }
            case EnemyPowers.InfectBoss:
                {
                    powerValue = 30;
                    break;
                }
        }

        player = _player;
        animationVirus.SetTrigger("Attack");
    }

    public void AttackAnimationEnd()
    {
        particleSystem.AttackEnemyPower(power, this, player);
        battleSystem.AttackEnemyEnd(powerValue, player.GetComponent<PlayerBattleSystem>());
    }

    public void AttackEnd()
    {
        battleSystem.AttackFXEnd();

        power = EnemyPowers.Invalid;
        player = null;
    }

    public void SetDamage(float dmg)
    {
        hp -= dmg;

        if (hp <= 0)
        {
            EnemyEndBattle();
        }
    }

    public float GetHP()  //Este metodo devuelve la vida del enemigo
    {
        return hp;
    }

    public void EnemyEndBattle()
    {
        //Acá hacemos el IF para comprobar que HP <= 0 para destruir el objeto virus
        if (hp <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            //TODO -> Reiniciar y cambiar los valores necesarios del enemigo cuando no esta en batalla. Esto es si el player abandona la batalla
        }
    }

}
