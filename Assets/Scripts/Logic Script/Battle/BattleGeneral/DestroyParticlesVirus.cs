﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticlesVirus : MonoBehaviour
{   
    public List<ParticleSystem> particlesVirus;

    void Start()
    {
        for(int i = 0; i < 5; i++)
        {
            if (PlayerPrefs.GetInt("DeathVirus" + i, 0) == 1)
            {
                particlesVirus[i].gameObject.SetActive(false);
            }
        }
    }
}
