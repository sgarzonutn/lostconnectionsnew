﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LifePlayer : ScriptableObject
{
    public float hpPlayerOne;
    public float manaPlayerOne;

    public float[] damagePowers;
    public float[] manaPowersCost;


    public void Load()
    {
        hpPlayerOne = SessionData.Data.hpPlayerOne;
        manaPlayerOne = SessionData.Data.manaPlayerOne;


        //Debug.Log(SessionData.Data.lifeHacker);
        SessionData.LoadData();
    }

    public void Upgrade()
    {
        SessionData.Data.hpPlayerOne = hpPlayerOne;
        SessionData.Data.manaPlayerOne = manaPlayerOne;
        SessionData.Data.manaPowersCost = manaPowersCost;
        SessionData.Data.damagePowers = damagePowers;

        //to save
        SessionData.SaveData();
    }

    public float GetPowerManaCost(PlayerBattleSystem.PlayerPowers power)
    {
        return manaPowersCost[(int)power];
    }

    public float GetPowerDamage(PlayerBattleSystem.PlayerPowers power)
    {
        return damagePowers[(int)power];
    }

}
