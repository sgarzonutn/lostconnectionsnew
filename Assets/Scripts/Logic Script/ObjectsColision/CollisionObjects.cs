﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class CollisionObjects : MonoBehaviour
{
    public ActivatePanelGeneral activatePanel;
    public TextPanelInformation txtPanelInformation;
    public LifePlayer lifePlayer;
    public Animator animator;

    private int language;
    private bool control;


    void Awake()
    {
        activatePanel = GameObject.Find("Canvas/TextBoxManager").GetComponent<ActivatePanelGeneral>();
        lifePlayer = Resources.Load("Prefabs/Characters/LifePlayer") as LifePlayer;
        txtPanelInformation = Resources.Load("Prefabs/Characters/Text Panel Information") as TextPanelInformation;
        


        language = PlayerPrefs.GetInt("LenguajeGuardado", 0);
        animator = gameObject.GetComponent<Animator>();


        control = false;
    }


    //Unicamente para este caso lo pongo
    private void Update()
    {
        if (control)
        {
            if (Input.GetKey(KeyCode.E))
            {
                lifePlayer.hpPlayerOne = 100;
                lifePlayer.manaPlayerOne = 100;
                lifePlayer.Upgrade();            

                Value(9);
                control = false;
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Tree":
                {
                    Value(1);
                    activatePanel.ActivatePanel();
                    control = true;
                }
                break;
            case "NoTree":
                Value(2);
                activatePanel.ActivatePanel();
                break;
            case "Arcade":
                if (PlayerPrefs.GetInt("ArcadeGame") == 0)
                {
                    Value(4);
                    activatePanel.ActivatePanel();
                }
                else
                {
                    Value(3);
                    activatePanel.ActivatePanel();
                }
                break;
            case "CollisionShop":
                if (PlayerPrefs.GetInt("TarjetaShop") == 0)
                {
                    Value(4);
                    activatePanel.ActivatePanel();
                }
                else
                {
                    Destroy(other.gameObject);
                }
                break;
            case "CollisionWorld": Value(5); activatePanel.ActivatePanel(); break;
            case "ShopLevelArc2": Value(6); activatePanel.ActivatePanel(); break;
            case "ShopLevelArc4": Value(7); activatePanel.ActivatePanel(); break;
            case "ShopLevelArc7": Value(8); activatePanel.ActivatePanel(); break;
            case "Panal":
                if (PlayerPrefs.GetInt("TarjetaPanal", 0) == 0)
                {
                    Value(4);
                    activatePanel.ActivatePanel();
                }
                else //BOSS BATTLE
                {
                    if (this.lifePlayer.hpPlayerOne < 90)
                    {
                        Value(10);
                        activatePanel.ActivatePanel();
                    } 
                    else
                    {
                        SavePosition.cargarPosicionInicial = 2;
                        SceneManager.LoadScene("BattleBoss");
                    }                    
                }
                break;
            case "EnemiesGroup":
                SavePosition.cargarPosicionInicial = 2;
                animator.SetBool("Walk", false);
                CardsCounterWorld cardsCounter = Resources.Load("Prefabs/Characters/Cards Counter World") as CardsCounterWorld;
                cardsCounter.UpgradeCards();

                PlayerPrefs.SetInt("ObjetoElegido", PlayerPrefs.GetInt("ObjetoElegido", 0));
                PlayerPrefs.SetInt("PlayerClass", PlayerPrefs.GetInt("PlayerClass", 0));

                PlayerPrefs.Save();

                //Debug.Log("Enemy number " + other.transform.parent.GetSiblingIndex());
                PlayerPrefs.SetInt("CurrentVirusNumber", other.transform.parent.GetSiblingIndex());
                SceneManager.LoadScene("BattleScene");
                break;
        }

    }


    private void OnTriggerExit(Collider other)
    {
        switch (other.tag)
        {
            case "Tree":
            case "NoTree":
            case "Arcade":
            case "CollisionShop":
            case "CollisionWorld":
            case "ShopLevelArc2":
            case "ShopLevelArc4":
            case "ShopLevelArc7":
            case "Panal":
                {
                    activatePanel.SetText("");
                    activatePanel.UnactivePanel();
                    break;
                }
        }
    }

    private void Value(int number)
    {
        if (language == 0)
            English(number);
        else
            Spanish(number);
    }


    private void Spanish(int number)
    {
        switch (number)
        {
            case 1: activatePanel.SetText(txtPanelInformation.treeSpanish); break;
            case 2: activatePanel.SetText(txtPanelInformation.noTreeSpanish); break;
            case 3: activatePanel.SetText(txtPanelInformation.noArcadeSpanish); break;
            case 4: activatePanel.SetText(txtPanelInformation.noAutorizeSpanish); break;
            case 5: activatePanel.SetText(txtPanelInformation.stopEnterSpanish); break;
            case 6: activatePanel.SetText(txtPanelInformation.shopArc2Spanish); break;
            case 7: activatePanel.SetText(txtPanelInformation.shopArc4Spanish); break;
            case 8: activatePanel.SetText(txtPanelInformation.shopArc7Spanish); break;
            case 9: activatePanel.SetText(txtPanelInformation.energyChargedSpanish); break;
            case 10: activatePanel.SetText(txtPanelInformation.notEnergySpanish); break;
        }
    }

    private void English(int number)
    {
        switch (number)
        {
            case 1: activatePanel.SetText(txtPanelInformation.treeEnglish); break;
            case 2: activatePanel.SetText(txtPanelInformation.noTreeEnglish); break;
            case 3: activatePanel.SetText(txtPanelInformation.noArcadeEnglish); break;
            case 4: activatePanel.SetText(txtPanelInformation.noAutorizeEnglish); break;
            case 5: activatePanel.SetText(txtPanelInformation.stopEnterEnglish); break;
            case 6: activatePanel.SetText(txtPanelInformation.shopArc2English); break;
            case 7: activatePanel.SetText(txtPanelInformation.shopArc4English); break;
            case 8: activatePanel.SetText(txtPanelInformation.shopArc7English); break;
            case 9: activatePanel.SetText(txtPanelInformation.energyChargedEnglish); break;
            case 10: activatePanel.SetText(txtPanelInformation.notEnergyEnglish); break;
        }
    }



}
