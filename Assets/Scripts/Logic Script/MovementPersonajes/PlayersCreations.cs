﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayersCreations : MonoBehaviour
{

    int pjSelected = 0;

    public List<GameObject> characterPrefabsList;

    enum PlayersEnum
    {
        Atif,
        Charlie,
        Dante
    }

    void Awake()
    {
        CreatePlayers();
    }

    void CreatePlayers()
    {
        pjSelected = PlayerPrefs.GetInt("ObjetoElegido", 0);  //0 Atif, 1 Charlie, 2 Dante
        Transform playersParent = GameObject.Find("ObjectsWorldScene/ObjectPlayers").transform;

        //Player 1 - PlayerController / MovementPlayerNewWorld
        GameObject userPlayer = GetPlayerPrefab((PlayersEnum)pjSelected);
        CreateUserPlayer(userPlayer, playersParent);

        //Player 2,3,n - NavMeshAgent / FollowPlayerTwo
        /*for(int i = 0; i<characterPrefabsList.Count; i++)
           {
               GameObject playerSupport = GetPlayerPrefab((PlayersEnum)1);

               if (playerSupport != userPlayer)
               {
                   CreateNonControllerPlayer(playerSupport, playersParent);
               }
           }*/


        //TPDO - ELIMINAR ESTE FOR Y DEJAR AL DE ARRIBA CUANDO ESTEN LISTOS LOS PREFABS DE LOS PLAYERS
        /*
        for (int i = 1; i < characterPrefabsList.Count; i++)
        {
            GameObject playerSupport = GetPlayerPrefab((PlayersEnum)1);
            CreateNonControllerPlayer(playerSupport, playersParent);
        }*/
    }

    private GameObject GetPlayerPrefab(PlayersEnum playerNumber)
    {
        return characterPrefabsList[(int)playerNumber];
    }

    private void CreateUserPlayer(GameObject prefab, Transform playersParent)
    {
        Transform player = Instantiate(prefab, playersParent).transform;
        player.parent = playersParent;

        if (SceneManager.GetActiveScene().name == "SampleScene")
        {
            player.gameObject.AddComponent<MovementPlayerNewWorld>().speed = 70; //Speed=70}

            CharacterController charController = player.gameObject.AddComponent<CharacterController>();

            charController.height = 20;
            charController.radius = 5;
            charController.slopeLimit = 95;
            charController.center = new Vector3(0f, 10f, 0f);


            player.GetChild(0).transform.position = player.position - new Vector3(0, -4f, 0);
            /*if (pjSelected == 0)
                 //Ajustamos la posicion del hijo (GameObject modelo del pj) por la escala                               
            else
                player.GetChild(0).transform.position = player.position - new Vector3(0, 4f, 0);*/


            player.gameObject.AddComponent<CollisionObjects>();
        }
        else
        {
            player.gameObject.AddComponent<PlayerBattleSystem>();
        }

        if(SceneManager.GetActiveScene().name == "BattleScene")
        {
            player.GetChild(0).transform.position = player.position - new Vector3(0, -10f, 0);
            
        }

         //Ajustamos la posicion del hijo (GameObject modelo del pj) por la escala                               

        player.tag = "Player"; //Solo el Player 1 tiene el tag Player
        player.name = prefab.name;
    }

    private void CreateNonControllerPlayer(GameObject prefab, Transform playersParent)
    {
        Transform player = Instantiate(prefab, playersParent).transform;
        player.parent = playersParent;
        player.name = prefab.name;
        NavMeshAgent agent = player.gameObject.AddComponent<NavMeshAgent>();
        FollowPlayerTwo followP = player.gameObject.AddComponent<FollowPlayerTwo>();
        followP.jugador = GameObject.Find("ObjectsWorldScene/ObjectPlayers").transform.GetChild(0);
        agent.baseOffset = 1;
        agent.speed = 70;
        agent.angularSpeed = 360;
        agent.acceleration = 50;
        agent.stoppingDistance = 40;
        agent.radius = 100;
        agent.height = 2;
    }
}
