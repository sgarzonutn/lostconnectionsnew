﻿using UnityEngine;


[CreateAssetMenu (menuName = "Shop/InventoryItem")]
public class InventoryItem : ScriptableObject
{
    public string itemName;
    public int id;
    public Sprite sprite;
    public int value;
    public Color backgroundColor;
    public int amount;
    public CardType cardType;
    private int _finalValue1;
    private int _finalValue2;
    private int _finalValue3;
    private int _finalValue4;
    private int _gameMoney;

    public void Load() {
        amount = SessionData.Data.cardsAmount[id];
        SessionData.LoadData();
        switch (id)
        {
            case 0:
               amount = PlayerPrefs.GetInt("violet", 0);
                break;
            case 1:
                amount = PlayerPrefs.GetInt("green", 0);
                break;
            case 2: 
                amount = PlayerPrefs.GetInt("blue", 0);
                break;
            case 3:
                amount = PlayerPrefs.GetInt("black", 0);
                break;
            case 4:
                amount = PlayerPrefs.GetInt("gold", 0);
                break;
        }
    }
    
    public void Upgrade() {

        SessionData.Data.cardsAmount[id] = amount;
        SessionData.SaveData();
        switch (id)
        {
            case 0:
                PlayerPrefs.SetInt("violet", amount);
                break;
            case 1:
                PlayerPrefs.SetInt("green", amount);
                break;
            case 2: 
                PlayerPrefs.SetInt("blue", amount);
                break;
            case 3:
                PlayerPrefs.SetInt("black", amount);
                break;
            case 4:
                PlayerPrefs.SetInt("gold", amount);
                break;
        }
    }

    

}
[System.Serializable]
public class InventoryItemSerializable {
    public int cardsAmount;
}
public enum CardType{HISTORY, INFO, BLACK, ID}