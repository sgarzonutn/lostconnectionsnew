﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    CardsCounterWorld cardsCounter;
    public List<PlayerBattleSystem.PlayerPowers> powersInventoryList;
    public List<PlayerBattleSystem.PlayerPowers> currentPowers;

    public Transform inventoryContent;
    public Transform currentContent;
    public List<Transform> currentPowersList;

    public GameObject powerDrag;
    Vector2 dragTempPosition;

    void Start()
    {
        //Init();
    }

    private void OnEnable()
    {
        Init();
    }

    void Init()
    {
        inventoryContent = transform.Find("PnlButtonsPower/Scroll View/Viewport/Content");
        currentContent = transform.Find("PnlData/PnlButtonPowerSold/Scroll View/Viewport/Content");

        cardsCounter = Resources.Load("Prefabs/Characters/Cards Counter World") as CardsCounterWorld;
        cardsCounter.Load();

        FillPowersInventoryList();
        FillCurrentPowersList();
    }

    private void OnDisable()
    {
        for (int i = 0; i < inventoryContent.childCount; i++)
        {
            Destroy(inventoryContent.GetChild(i).gameObject);
        }

        for (int i = 0; i < currentContent.childCount; i++)
        {
            Destroy(currentContent.GetChild(i).gameObject);
        }
    }

    void FillPowersInventoryList()
    {
        powersInventoryList = cardsCounter.powersInventoryList;

        for (int i = 0; i < powersInventoryList.Count; i++)
        {
            Transform power = Instantiate(powerDrag).transform;
            power.transform.parent = inventoryContent;
            power.transform.GetChild(0).GetComponent<Text>().text = powersInventoryList[i].ToString();

            DragAndDrop(power);
        }
    }

    void FillCurrentPowersList()
    {
        currentPowers = cardsCounter.currentPowers;

        for (int i = 0; i < currentPowers.Count; i++)
        {
            Transform power = Instantiate(powerDrag).transform;
            power.parent = currentContent;
            power.transform.GetChild(0).GetComponent<Text>().text = currentPowers[i].ToString();

            currentPowersList.Add(power);
        }
    }

    void DragAndDrop(Transform power)
    {
        EventTrigger trigger = power.GetComponents<EventTrigger>()[0];
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => { FollowDrag(power); });
        trigger.triggers.Add(entry);

        EventTrigger trigger2 = power.GetComponents<EventTrigger>()[1];
        EventTrigger.Entry entry2 = new EventTrigger.Entry();
        entry2.eventID = EventTriggerType.PointerUp;
        entry2.callback.AddListener((data) => { PowerDrop(power); });
        trigger2.triggers.Add(entry2);
    }

    void FollowDrag(Transform power)
    {
        power.parent = transform;
        power.position = Input.mousePosition;
    }

    void PowerDrop(Transform power)
    {
        Rect rectPower = new Rect(power.position.x, power.position.y, power.GetComponent<RectTransform>().rect.width, power.GetComponent<RectTransform>().rect.height);
        List<Rect> rectPowers = new List<Rect>();

        for (int i = 0; i < currentPowersList.Count; i++)
        {
            Rect rect = new Rect(currentPowersList[i].position.x, currentPowersList[i].position.y, currentPowersList[i].GetComponent<RectTransform>().rect.width, currentPowersList[i].GetComponent<RectTransform>().rect.height);
            rectPowers.Add(rect);               
        }

        for (int i = 0; i < rectPowers.Count; i++)
        {
            if (rectPower.Overlaps(rectPowers[i]))
            {
                for (int idx = 0; idx < (int)PlayerBattleSystem.PlayerPowers.Invalid; idx++)
                {
                    PlayerBattleSystem.PlayerPowers powerString = (PlayerBattleSystem.PlayerPowers)idx;

                    if (power.GetChild(0).GetComponent<Text>().text == powerString.ToString())
                    {
                        if (ReplaceCurrentPower(i, powerString))
                        {
                            currentPowersList[i].GetChild(0).GetComponent<Text>().text = power.GetChild(0).GetComponent<Text>().text;
                        }
                    }
                }

                break;
            }
        }

        power.parent = inventoryContent;
    }

    bool ReplaceCurrentPower(int index, PlayerBattleSystem.PlayerPowers power)
    {
        return cardsCounter.UpdateCurrentPower(index, power);
    }
}
