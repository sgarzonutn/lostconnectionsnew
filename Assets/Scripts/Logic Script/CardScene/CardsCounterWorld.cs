﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CardsCounterWorld : ScriptableObject
{
    public int cardBlackAmount;
    public int cardGoldenAmount;
    public int cardGreenAmount;
    public int cardVioletAmount;
    public int cardBlueAmount;

    public List<PlayerBattleSystem.PlayerPowers> currentPowers;
    public List<PlayerBattleSystem.PlayerPowers> powersInventoryList;

    public void Load()
    {
        SessionData.LoadData();

        cardBlackAmount = SessionData.Data.cardBlackAmount;
        cardGoldenAmount = SessionData.Data.cardGoldenAmount;
        cardGreenAmount = SessionData.Data.cardGreenAmount;
        cardVioletAmount = SessionData.Data.cardVioletAmount;
        cardBlueAmount = SessionData.Data.cardBlueAmount;

        currentPowers = SessionData.Data.currentPowers;
        powersInventoryList = SessionData.Data.powersInventoryList;

        //Debug.Log(SessionData.Data.lifeHacker);
    }

    public void UpgradeCards()
    {
        SessionData.Data.cardBlackAmount = cardBlackAmount;
        SessionData.Data.cardGoldenAmount = cardGoldenAmount;
        SessionData.Data.cardGreenAmount = cardGreenAmount;
        SessionData.Data.cardVioletAmount = cardVioletAmount;
        SessionData.Data.cardBlueAmount = cardBlueAmount;

        //to save
        SessionData.SaveData();
    }

    public void AddInventoryPower(PlayerBattleSystem.PlayerPowers power)
    {
        powersInventoryList = SessionData.Data.powersInventoryList;

        bool found = false;

        for (int i = 0; i < powersInventoryList.Count; i++)
        {
            if (power == powersInventoryList[i])
            {
                found = true;

                break;
            }
        }

        if (!found)
        {
            powersInventoryList.Add(power);

            SessionData.Data.powersInventoryList = powersInventoryList;

            //to save
            SessionData.SaveData();
        }
    }

    public bool UpdateCurrentPower(int index, PlayerBattleSystem.PlayerPowers power)
    {
        if (currentPowers.Count < 3)
        {
            currentPowers.Add(power);
        }
        else
        {
            for (int idx = 0; idx < currentPowers.Count; idx++)
            {
                if (currentPowers[idx] == power)
                {
                    return false;
                }
            }

            List<PlayerBattleSystem.PlayerPowers> powersTemp = new List<PlayerBattleSystem.PlayerPowers>();

            for (int i = 0; i < currentPowers.Count; i++)
            {
                if (i == index)
                {
                    powersTemp.Add(power);
                }
                else
                {
                    powersTemp.Add(currentPowers[i]);
                }
            }

            currentPowers.Clear();
            currentPowers = powersTemp;
        }

        SessionData.Data.currentPowers = currentPowers;

        //to save
        SessionData.SaveData();

        return true;
    }

    public void Reset()
    {
        SessionData.ClearData();
    }
}
