﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cardCounter : MonoBehaviour
{

    public Text blackCards;
    public CardsCounterWorld cardsCounterw;


    // Update is called once per frame
    void Update()
    {
        float blackPoint = cardsCounterw.cardBlackAmount * 100;
        float goldenPoint = cardsCounterw.cardGoldenAmount * 10;

        string value = blackPoint + "\n" + goldenPoint;

        blackCards.text = value;
        
    }
}
