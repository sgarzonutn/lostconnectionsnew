﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticlesCards : MonoBehaviour
{
    public List<ParticleSystem> particles;
    public List<GameObject> cards;
    public List<ParticleSystem> blockAccess;
    public List<GameObject> cardsBlackGB;
    public List<GameObject> particlesCardsGB;

    void Start()
    {
        for(int i = 0; i < 6; i++)
        {
            if (PlayerPrefs.GetInt("CardNumber" + (i + 1), 0) == 1)
            {
                particles[i].gameObject.SetActive(false);
                Destroy(cards[i]);
            }
        }

        for(int i = 0; i < 7; i++)
        {
            if (PlayerPrefs.GetInt("valueCardGB" + (i + 1), 0) == 1)
            {
                cardsBlackGB[i].gameObject.SetActive(false);
                Destroy(cardsBlackGB[i]);
            }
        }


        if (PlayerPrefs.GetInt("ArcadeGame", 0) == 1)
            blockAccess[0].gameObject.SetActive(false);

        if (PlayerPrefs.GetInt("TarjetaPanal", 0) == 1)
            blockAccess[1].gameObject.SetActive(false);

        if (PlayerPrefs.GetInt("TarjetaShop", 0) == 1)
            blockAccess[2].gameObject.SetActive(false);


    }





}
