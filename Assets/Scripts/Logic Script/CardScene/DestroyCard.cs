﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyCard : MonoBehaviour
{

    /*
    * Colores
    * Color verde: 3 y 2
    * Color azul: 4, 5 y  6
    * Color violeta: 1
    * 
    * Pero esto va en el otro Script CARD SCORE GB
    * Color dorada: 2
    * Color negra: 1
    */

    [SerializeField] [Range(1, 6)] public int cardValue;
    public GameObject panel;
    public Texture spanishTexture;
    public Texture englishTexture;
    public RawImage imageTexture;

    public ParticleSystem padlockBridge;
    public ParticleSystem padlockArcade;
    public ParticleSystem padlockPanal;

    public InventoryItem Card;
    private InventoryScript _inventoryScript;

    public GameObject inventary;
    public ParticleSystem particlesCard;

    public GameObject panelCollisionOpenWorld;

    public CardsCounterWorld cardsPrueba;


    private CameraPlayer camPlayer;

    private void Start()
    {
        _inventoryScript = inventary.GetComponent<InventoryScript>();
        camPlayer = Camera.main.GetComponent<CameraPlayer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            MovementPlayerNewWorld player = other.gameObject.GetComponent<MovementPlayerNewWorld>();
            player.SetMovementPlayer(false);

            camPlayer.setCameraMovement(false);
            
            if(panelCollisionOpenWorld.activeSelf)
                panelCollisionOpenWorld.SetActive(false);

            if (this.VerificarLenguaje() == 0)
            {
                this.imageTexture.texture = this.englishTexture;
            }
            else
            {
                this.imageTexture.texture = this.spanishTexture;
            }

            this.gameObject.SetActive(true);
            panel.gameObject.SetActive(true);


            Card.amount += 1;  //Esto era el contador de tarjetas, verificar si lo puse bien

            _inventoryScript.LoadCards();


            particlesCard.gameObject.SetActive(false);
            PlayerPrefs.SetInt("CardNumber" + cardValue, 1);
            Destroy(this.gameObject);

            switch (cardValue)
            {
                case 1:
                    cardsPrueba.cardVioletAmount += 1;
                    break;
                case 2:
                    cardsPrueba.cardGreenAmount += 1; 
                    break;
                case 3:
                    cardsPrueba.cardGreenAmount += 1;
                    break;
                case 4:
                    PlayerPrefs.SetInt("ArcadeGame", 1);
                    cardsPrueba.cardBlueAmount += 1;
                    Destroy(padlockArcade);
                    break;
                case 5:
                    PlayerPrefs.SetInt("TarjetaPanal", 1);
                    cardsPrueba.cardBlueAmount += 1;
                    Destroy(padlockPanal);
                    break;
                case 6:
                    PlayerPrefs.SetInt("TarjetaShop", 1);                    
                    cardsPrueba.cardBlueAmount += 1;
                    Destroy(padlockBridge);
                    break;                    
            }
            Card.Upgrade(); //para guardar en playerpref
            cardsPrueba.UpgradeCards();



        }
    }


    private int VerificarLenguaje()
    {
        int valor = 0;

        if (PlayerPrefs.HasKey("LenguajeGuardado"))
            valor = PlayerPrefs.GetInt("LenguajeGuardado", 0);

        return valor;
    }
}