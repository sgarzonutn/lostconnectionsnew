﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateText : MonoBehaviour
{
	public TextAsset englishText;
	public TextAsset espanolText;

	public GameObject objetoTextBoxManager;

	private TextBoxManager theTextBox;
	private MovementPlayerNewWorld movPlayer;

	public int lineaInicio;
	public int lineaFin;
	public int tipoNPC;

	private bool requieredMouseClick;
	private bool waitingClick;


	void Start()
	{
		theTextBox = this.objetoTextBoxManager.GetComponent<TextBoxManager>();
		this.requieredMouseClick = true;
	}


	void Update()
	{
		if (!theTextBox.isActive && waitingClick && Input.GetMouseButtonDown(0) &&
			!theTextBox.getActiveShop() && !theTextBox.getActiveButon())
			this.dataTextGame();
	}


	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			movPlayer = other.GetComponent<MovementPlayerNewWorld>();
			movPlayer.SetMovementPlayer(false);
			theTextBox.SetMovementPlayer(movPlayer);
			theTextBox.SetColisionGuard(true);
			other.gameObject.GetComponent<Animator>().SetBool("Walk", false);

			this.dataTextGame();

			if (requieredMouseClick)
			{
				waitingClick = true;
				return;
			}
		}
	}


	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			waitingClick = false;
			theTextBox.SetActiveShop(false);
			theTextBox.SetColisionGuard(false);
		}
	}


	private int getValidateLanguage()
	{
		int language = PlayerPrefs.GetInt("LenguajeGuardado", 0);
		return language;
	}


	private void dataTextGame()
	{

		if (this.getValidateLanguage() == 0)
			theTextBox.ReloadScript(englishText);
		else
			theTextBox.ReloadScript(espanolText);

		theTextBox.setTipoNPC(tipoNPC);
		theTextBox.currentLine = lineaInicio;
		theTextBox.endAtLine = lineaFin;
		theTextBox.EnableTextBox();


		if (theTextBox.getActiveShop())
		{
			waitingClick = false;
		}
	}


	public void setTipoNPC(int valor)
	{
		this.tipoNPC = valor;
	}

	public void SetWaitingClick(bool value)
	{
		waitingClick = value;
	}

}
