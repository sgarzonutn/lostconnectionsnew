﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ValidatorNamePlayer : MonoBehaviour
{
    public Button btnPlayGame;
    public GameObject panelValidator;
    public Text personajeElegido;
    public InputField txtNombreIngresado;
    public Texture textureImage;
    public Text txtInformation;
    public RawImage imagenPanel;
    public List<string> textInformationPanel = new List<string>();
    public LifePlayer lifePlayer;

    private void Awake()
    {
        textInformationPanel.Add("<Please enter player name or your progress will not be saved>");
        textInformationPanel.Add("<Por favor ingrese nombre de jugador sino su progreso no será guardado>");
        textInformationPanel.Add("<Dante is not available, select another character>");
        textInformationPanel.Add("<Dante no esta disponible, seleccione otro personaje>");

        lifePlayer = Resources.Load("Prefabs/Characters/LifePlayer") as LifePlayer;
    }


    public void ValidarNombreIngresado()
    {
        if(!ValidateNoDante())
        {
            if (this.txtNombreIngresado.text.Equals(""))
            {
                if (!PlayerPrefs.HasKey("LenguajeGuardado"))
                {
                    PlayerPrefs.SetInt("LenguajeGuardado", 0);
                }

                SetTextInformation(textInformationPanel[0].ToString(), textInformationPanel[1].ToString());

                imagenPanel.texture = textureImage;
                this.panelValidator.SetActive(true);


                return;
            }

            SetearTodo();

            SceneManager.LoadScene("SampleScene");
        }
    }

    private bool ValidateNoDante()
    {
        bool control = false;

        if(PlayerPrefs.GetInt("ObjetoElegido", 0) == 2)
        {
            SetTextInformation(textInformationPanel[2].ToString(), textInformationPanel[3].ToString());

            imagenPanel.texture = textureImage;
            this.panelValidator.SetActive(true);

            control = true;
        }

        return control;
    }


    public void ClosePanelValidation()
    {
        panelValidator.SetActive(false);
    }


    private void SetTextInformation(string textEspanish, string textEnglish)
    {
        int valor = PlayerPrefs.GetInt("LenguajeGuardado", 0);

        if (valor == 0)
            this.txtInformation.text = textEspanish;
        else
            this.txtInformation.text = textEnglish;
    }

    public void SetearTodo()
    {
        PlayerPrefs.SetInt("ValorGuardadoTarjeta", 0);
        PlayerPrefs.SetInt("TarjetaShop", 0);
        PlayerPrefs.SetInt("ArcadeGame", 0);
        PlayerPrefs.SetInt("TarjetaPanal", 0);

        for(int i = 0; i < 5; i++)
            PlayerPrefs.SetInt("DeathVirus" + i, 0);

        for(int i = 0; i < 6; i++)        
            PlayerPrefs.SetInt("CardNumber" + (i + 1), 0);

        for(int i = 1; i < 8; i++)
            PlayerPrefs.SetInt("valueCardGB" + i, 0);


        PlayerPrefs.Save();




        lifePlayer.hpPlayerOne = 100;
        lifePlayer.manaPlayerOne = 100;
        lifePlayer.Upgrade();

        CardsCounterWorld cardsPrueba = Resources.Load("Prefabs/Characters/Cards Counter World") as CardsCounterWorld;
        cardsPrueba.Reset();
    }

    

}
