﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardsInventory : MonoBehaviour
{ 
    public RawImage imgInventoryCards;
    public List<Texture> textureSpanish;
    public List<Texture> textureEnglish;
    public Texture textureDefault;

    public CardsCounterWorld cardsCounterNW;

    public Text txtCountGreen;
    public Text txtCountBlue;
    public Text txtCountViolet;

    private int valueBlue;
    private int valueGreen;

    void Awake()
    {
        valueBlue = 0;
        valueGreen = 3;
    }

    public void SetCards()
    {
        txtCountBlue.text = cardsCounterNW.cardBlueAmount.ToString();
        txtCountGreen.text = cardsCounterNW.cardGreenAmount.ToString();
        txtCountViolet.text = cardsCounterNW.cardVioletAmount.ToString();
    }

    public void SetChangeImageBlueCards()
    {
        ImageLangugeSelected(valueBlue);
        valueBlue++;

        if(valueBlue >= 3)
        {
            valueBlue = 0;
        }
    }

    public void SetChangeImageGreenCards()
    {
        ImageLangugeSelected(valueGreen);
        valueGreen++;

        if (valueGreen >= 5)
        {
            valueGreen = 3;
        }
    }

    public void SetChangeImageVioletCards()
    {
        ImageLangugeSelected(5);
    }

    public void ImageLangugeSelected(int valueImage)
    {
        int value = PlayerPrefs.GetInt("LenguajeGuardado", 0);

        if (value == 0)        
            imgInventoryCards.texture = textureEnglish[valueImage];
        else 
            imgInventoryCards.texture = textureSpanish[valueImage];
        
    }

    public void SetTextureDefault()
    {
        imgInventoryCards.texture = textureDefault;
    }





}
