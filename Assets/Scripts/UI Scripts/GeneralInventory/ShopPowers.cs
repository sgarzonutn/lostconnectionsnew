﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPowers : MonoBehaviour
{
    public GameObject pnlAtif;
    public GameObject pnlCharlie;
    public Text txtInformationPower;

    public CardsCounterWorld cardsCounter;

    public Button[] powerGeneral;

    void Awake()
    {
        int playerSelected = PlayerPrefs.GetInt("ObjetoElegido", 0);

        if (playerSelected == 0)
            pnlAtif.gameObject.SetActive(true);
        else
            pnlCharlie.gameObject.SetActive(true);
    }

    public void PowerSelected(int _power)
    {
        //Las tarjetas negras valen 100 y las tarjetas doradas 10

        bool value = false;
        PlayerBattleSystem.PlayerPowers power = (PlayerBattleSystem.PlayerPowers)_power;

        switch (power)
        {
            case PlayerBattleSystem.PlayerPowers.Electroshock: //Electroshock  100 black  50 golden  
                if (cardsCounter.cardGoldenAmount >= 5 && cardsCounter.cardBlackAmount >= 1)
                {
                    cardsCounter.cardGoldenAmount -= 5;
                    cardsCounter.cardBlackAmount -= 1;
                    value = true;
                }

                break;

            case PlayerBattleSystem.PlayerPowers.Updating: //Updating 100 Black 60 Golden
                if (cardsCounter.cardGoldenAmount >= 6 && cardsCounter.cardBlackAmount >= 1)
                {
                    cardsCounter.cardGoldenAmount -= 6;
                    cardsCounter.cardBlackAmount -= 1;
                    value = true;
                }

                break;

            case PlayerBattleSystem.PlayerPowers.Heal: //Healing  200 black  20 golden
                if (cardsCounter.cardGoldenAmount >= 2 && cardsCounter.cardBlackAmount >= 2)
                {
                    cardsCounter.cardGoldenAmount -= 2;
                    cardsCounter.cardBlackAmount -= 2;
                    value = true;
                }

                break;


            case PlayerBattleSystem.PlayerPowers.Reset://Restart   200 black  20 golden
                if (cardsCounter.cardGoldenAmount >= 2 && cardsCounter.cardBlackAmount >= 2)
                {
                    cardsCounter.cardGoldenAmount -= 2;
                    cardsCounter.cardBlackAmount -= 2;
                    value = true;
                }

                break;

            case PlayerBattleSystem.PlayerPowers.ControlZ: //Control Z   100 black  50 golden
                if (cardsCounter.cardGoldenAmount >= 5 && cardsCounter.cardBlackAmount >= 1)
                {
                    cardsCounter.cardGoldenAmount -= 5;
                    cardsCounter.cardBlackAmount -= 1;
                    value = true;
                }

                break;

            case PlayerBattleSystem.PlayerPowers.Delete: //Delete  100 Black 60 Golden
                if (cardsCounter.cardGoldenAmount >= 6 && cardsCounter.cardBlackAmount >= 1)
                {
                    cardsCounter.cardGoldenAmount -= 6;
                    cardsCounter.cardBlackAmount -= 1;
                    value = true;
                }

                break;
        }

        SetText(value, power);
    }

    private void SetText(bool control, PlayerBattleSystem.PlayerPowers power)
    {
        if (control)
        {
            SetTextPower("VENDIDO!", "SOLD!");
            SetInventory(power);
        }
        else
        {   
            SetTextPower("NO TIENES SUFICIENTES TARJETAS", "YOU DON´T HAVE ENOUGH CARDS");
        }
    }

    private void SetInventory(PlayerBattleSystem.PlayerPowers power)
    {
        cardsCounter.AddInventoryPower(power);
    }

    private void SetTextPower(string spanish, string english)
    {
        int value = PlayerPrefs.GetInt("LenguajeGuardado", 0);

        if (value == 0)
            txtInformationPower.text = english;
        else
            txtInformationPower.text = spanish;
    }


}
