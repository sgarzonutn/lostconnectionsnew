﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateEnergySlider : MonoBehaviour
{
    
    public LifePlayer lifeP;
    public Slider sldEnergyPO;
    public Slider sldManaPO;

    private float lifeAux;
    private float manaAux;

    void Start()
    {
        lifeAux = -1.0f;
        manaAux = -1.0f;    
    }


    //Sacar este Update
    void Update()
    {

        if (lifeAux != lifeP.hpPlayerOne)
        {
            lifeAux = lifeP.hpPlayerOne;
            sldEnergyPO.value = lifeP.hpPlayerOne;            
        }

        if(manaAux != lifeP.manaPlayerOne)
        {
            manaAux = lifeP.manaPlayerOne;
            sldManaPO.value = lifeP.manaPlayerOne;
        }
    }
    
}
