﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoPanel : MonoBehaviour
{
    public Image imgPlayer;

    public Sprite imgCharlie;
    public Sprite imgAtif;

    void Start()
    {
        int playerSelected = PlayerPrefs.GetInt("ObjetoElegido", 0);
        imgPlayer.sprite = playerSelected == 0 ? imgAtif : imgCharlie;
    }
}
