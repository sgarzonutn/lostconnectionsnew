﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBoxManager : MonoBehaviour
{
	public GameObject textBox;
	public GameObject textBoxInformation;
	public GameObject objetoPanelShop;

	public Text theText;
	public Text theTextInformation;

	public Button btnYes;
	public Button btnNo;
	public Button btnIgnore;

	public CardsCounterWorld cardsPrueba;

	private MovementPlayerNewWorld movPlayer;
	private TextAsset textFile;

	public string[] textLines;

	public int valorAsset;
	public int currentLine;
	public int endAtLine;

	public bool isActive;
	private bool isActiveShop;
	private bool isActiveButton;
	private bool collisionGuard;

	private int tipoNPC;
	private int lineasNPCViejita;
	private int lineasNPCGuardia;
	public int valorActivadorBotones;


	void Awake()
	{
		
		this.collisionGuard = false;
		UnactiveButton();
	}


	void Start()
	{
		if (textFile != null)
			textLines = (textFile.text.Split('\n'));

		if (endAtLine == 0)
			endAtLine = textLines.Length - 1;

		if (isActive)
			EnableTextBox();
		else
			DisableTextBox();

		isActiveShop = false;

	}

	void Update()
	{
		if (isActive && Input.GetMouseButtonDown(0) && !getActiveButon() && collisionGuard
			&& !isActiveShop)
			advanceTextBox();
	}

	public void advanceTextBox()
	{
		currentLine += 1;

		if (currentLine > endAtLine)
		{
			DisableTextBox();

			movPlayer.SetMovementPlayer(true);
			StartCoroutine(UnactivatePanelCorrutina());
		}
		else
		{
			theText.text = textLines[currentLine];
			theTextInformation.text = textLines[currentLine];
		}

		if (currentLine == 4 && tipoNPC == 1)
			ActivateButton();
		else
			UnactiveButton();
	}

	public void EnableTextBox()
	{
		if (tipoNPC == 1) //La vieja
		{
			textBox.gameObject.SetActive(true);
			theText.text = textLines[currentLine];
		}
		else
		{
			textBoxInformation.gameObject.SetActive(true);
			theTextInformation.text = textLines[currentLine];
		}

		isActive = true;
	}


	public void SetMovementPlayer(MovementPlayerNewWorld mov)
	{
		movPlayer = mov;
	}

	public void DisableTextBox()
	{
		textBox.SetActive(false);
		textBoxInformation.SetActive(false);

		valorActivadorBotones = 0;
		theText.text = "";


		if (objetoPanelShop != null && lineasNPCGuardia == 2)
		{
			objetoPanelShop.SetActive(true);
			isActiveShop = true;
		}

	}

	public void ReloadScript(TextAsset theText)
	{
		if (theText != null)
		{
			textLines = new string[1];
			textLines = (theText.text.Split('\n'));
		}
	}

	public void setTipoNPC(int valorNPC)
	{
		this.tipoNPC = valorNPC;

		switch (tipoNPC)
		{
			case 1:
				lineasNPCViejita = 1;
				lineasNPCGuardia = 0;
				break;
			case 2:
				lineasNPCGuardia = 2;
				lineasNPCViejita = 0;
				break;
		}

	}

	public void UnactiveButton()
	{
		btnYes.gameObject.SetActive(false);
		btnNo.gameObject.SetActive(false);
		btnIgnore.gameObject.SetActive(false);

		isActiveButton = false;
	}

	public void ActivateButton()
	{
		btnYes.gameObject.SetActive(true);
		btnNo.gameObject.SetActive(true);
		btnIgnore.gameObject.SetActive(true);

		isActiveButton = true;
	}


	public bool getActiveShop()
	{
		return isActiveShop;
	}

	public void SetActiveShop(bool value)
	{
		isActiveShop = value;
	}

	public void ButtonPress(int value)
	{
		switch (value)
		{
			case (1):
				endAtLine = 0;
				break;
			case (2):
				isActiveButton = false;
				endAtLine = 7;
				break;
			case (3):
				isActiveButton = false;
				cardsPrueba.cardGoldenAmount += 1;
				endAtLine = 8;
				currentLine = 7;
				break;
		}

		advanceTextBox();
	}

	public bool getActiveButon()
	{
		return isActiveButton;
	}

	IEnumerator UnactivatePanelCorrutina()
	{
		yield return new WaitForSeconds(2);
		isActiveShop = false;
	}


	public void SetColisionGuard(bool val)
	{
		collisionGuard = val;
	}



}

