﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.U2D;

public class PanelUIBattle : MonoBehaviour
{

    public Button btnBack, btnPrefab, btnWinGameOver, btnQuitGame;

    public Image hpBarEnemy;
    public Image hpBarPlayer;
    public Image manaBarPlayer;

    public Image imgPlayer;
    public Image imgEnemy;

    public Sprite imgCharlie;
    public Sprite imgAtif;
    public Sprite imgVirus;

    float hpEnemyMax;
    float hpPlayerMax;
    float manaPlayerMax;

    public RawImage imgGameOver;
    public RawImage imgWinGame;

    public Text txtInformationBattle;

    public Transform scrollContent;

    public LifePlayer lifePlayer;

    int lang;

    void Start()
    {
        /*
        btnBack = transform.Find("ContainerBtn/BtnBack").GetComponent<Button>();

        scrollContent = btnBack.transform.parent.Find("Scroll View/Viewport/Content");

        btnPrefab = Resources.Load<Button>("Prefabs/UI/BtnPowerBattleSystem");


        //Esto es la llamada a los paneles que se desean activar o desactivar
        btnMenu = GameObject.Find("InventoryCanvas/OpenMenu");

        //Esto es para ir descontando las barras de energia
        sldEnemy = GameObject.Find("Canvas/PnlBattleUI/ImageVirus1/SldVirusOne").GetComponent<Slider>();
        sldPlayerOne = GameObject.Find("Canvas/PnlTarjetaGB/ImageCharlie/SldCharlie").GetComponent<Slider>();
        sldPlayerTwo = GameObject.Find("Canvas/PnlTarjetaGB/ImgAtif/SldAtif").GetComponent<Slider>();

        txtInformationBattle = GameObject.Find("Canvas/PnlBattleUI/TxtInformationBattle").GetComponent<Text>();

        imgCardBlack = GameObject.Find("Canvas/PnlTarjetaGB/Black").GetComponent<RawImage>();
        imgCardGolden = GameObject.Find("Canvas/PnlTarjetaGB/Golden").GetComponent<RawImage>();

        TxtTarjetaBlack = GameObject.Find("Canvas/PnlTarjetaGB/TxtTarjetaBlack").GetComponent<Text>();
        TxtTarjetaGolden = GameObject.Find("Canvas/PnlTarjetaGB/TxtTarjetaGolden").GetComponent<Text>();
        */
        //DEBUG = ONLY test
        //PlayerPrefs.SetInt("LenguajeGuardado", 0);

        lifePlayer = Resources.Load("Prefabs/Characters/LifePlayer") as LifePlayer;

        lang = PlayerPrefs.GetInt("LenguajeGuardado");

        int playerSelected = PlayerPrefs.GetInt("ObjetoElegido", 0);

        imgPlayer.sprite = playerSelected == 0 ? imgAtif : imgCharlie;

        if (imgGameOver != null)
            imgGameOver.enabled = false;

        if (imgWinGame != null)
            imgWinGame.enabled = false;

        if (btnWinGameOver != null)
            btnWinGameOver.gameObject.SetActive(false);

        if (btnQuitGame != null)
            btnQuitGame.gameObject.SetActive(false);

    }

    private void FillButton(GameObject button, PlayerBattleSystem player, PlayerBattleSystem.PlayerPowers power, Transform enemy)
    {
        //Image
        button.GetComponent<Image>().sprite = GetButtonImage(power);
        //OnClick
        button.GetComponent<Button>().onClick.AddListener(() => player.Attack(power, enemy));
        //Name
        button.name = power.ToString();
        //Event Trigger - Show Mana Cost
        string powerInfo = getTextEventButton(button, power);

        EventTrigger trigger = button.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((data) => { SetTextInformation(powerInfo); });
        entry.callback.AddListener((data) => { SetButtonInteractable(lifePlayer.GetPowerManaCost(power), player.GetMana(), button.GetComponent<Button>()); });
        trigger.triggers.Add(entry);
    }

    private Sprite GetButtonImage(PlayerBattleSystem.PlayerPowers power)
    {
        Sprite image = null;
        string powerName = power.ToString();

        if (lang == 0)
        {
            //0 es en Ingles
            image = Resources.Load<Sprite>("Textures/TextureBattleButton/English/" + powerName);
        }
        else
        {
            //1 en Español
            image = Resources.Load<Sprite>("Textures/TextureBattleButton/Espanol/" + powerName);
        }

        return image;
    }

    public void ShowAttackButtons(PlayerBattleSystem player, Transform enemy)
    {
        List<PlayerBattleSystem.PlayerPowers> powersList = player.GetPowers();

        for (int i = 0; i < player.GetPowers().Count; i++)
        {
            GameObject button = Instantiate(btnPrefab.gameObject);
            button.transform.parent = scrollContent;

            FillButton(button, player, powersList[i], enemy);
        }
    }

    public void CleanAttackButtons()
    {
        for (int i = 0; i < scrollContent.childCount; i++)
        {
            Destroy(scrollContent.GetChild(i).gameObject);
        }
    }

    //Esto es para desactivar los paneles cuando inicia la batalla

    public void SetScapeButton(BattleMachine battleSystem)
    {
        btnBack.onClick.AddListener(() => battleSystem.BattleEnd());
    }

    //Este método es para descontar las barra de energia del enemigo en la escena
    public void SetEnergyEnemy(float enemyHp)
    {
        if (hpEnemyMax == 0)
        {
            hpEnemyMax = enemyHp;
        }

        hpBarEnemy.fillAmount = (enemyHp * 100 / hpEnemyMax) / 100;
    }

    public void SetTextInformation(string dato)
    {
        txtInformationBattle.text = dato;
    }

    public void SetButtonInteractable(float manaCost, float playerMana, Button button)
    {
        float manaTemp = (manaCost * -1);

        if (manaTemp > playerMana)
        {
            button.interactable = false;
        }
        else
        {
            button.interactable = true;
        }
    }

    public void SetEnergyPlayer(float playerHp)
    {
        if(playerHp == 100)
        {
            if (hpPlayerMax == 0)
            {
                hpPlayerMax = playerHp;
            }

            hpBarPlayer.fillAmount = (playerHp * 100 / hpPlayerMax) / 100;
        }
        else
        {
            hpBarPlayer.fillAmount = (playerHp) / 100;
        }
        
    }

    public void SetManaPlayer(float playerMana)
    {
        if(manaPlayerMax == 100)
        {
            if (manaPlayerMax == 0)
            {
                manaPlayerMax = playerMana;
            }

            manaBarPlayer.fillAmount = (playerMana * 100 / manaPlayerMax) / 100;
        }
        else
        {
            manaBarPlayer.fillAmount = (playerMana) / 100;
        }
    }

    public void ActiveImgGameOver()
    {
        if (imgGameOver != null)
        {
            imgGameOver.enabled = true;
            ClearComponentsUI();
        }
            
    }

    public void ActivaImgWinGame()
    {
        if (imgWinGame != null && !imgGameOver.gameObject.activeSelf)
        {
            imgWinGame.enabled = true;
            ClearComponentsUI();
        }   
    }

    private void ClearComponentsUI()
    {
        btnWinGameOver.gameObject.SetActive(true);
        btnQuitGame.gameObject.SetActive(true);
    }

    public void DisableButtonBack()
    {
        btnBack.enabled = false;
    }

    public string getTextEventButton(GameObject buttonPower, PlayerBattleSystem.PlayerPowers _powerCurrent)
    {
        string powerInfo = "";
        int x = PlayerPrefs.GetInt("LenguajeGuardado", 0);

        if (x == 0)
            powerInfo = buttonPower.name + " - DMG: " + lifePlayer.GetPowerDamage(_powerCurrent).ToString() +
                " / Mana Cost: " + lifePlayer.GetPowerManaCost(_powerCurrent).ToString();
        else
            powerInfo = buttonPower.name + " - Daño al Enemigo: " + lifePlayer.GetPowerDamage(_powerCurrent).ToString() +
                " / Costo de Mana: " + lifePlayer.GetPowerManaCost(_powerCurrent).ToString();

        return powerInfo;
    }




}
