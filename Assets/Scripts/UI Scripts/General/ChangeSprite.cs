﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSprite : MonoBehaviour
{
    public Sprite imageEnglish;
    public Sprite imageSpanish;
    public Button btnImage;
    public GameObject pnlImage;

    private void Start()
    {
        int valor = PlayerPrefs.GetInt("LenguajeGuardado", 0);

        if (valor == 0)
        {
            //0 es en Ingles
            if(btnImage != null)
                btnImage.GetComponent<Image>().sprite = imageEnglish;

            if(pnlImage != null)
                pnlImage.GetComponent<Image>().sprite = imageEnglish;
        }
        else
        {
            //1 en Español
            if (btnImage != null)
                btnImage.GetComponent<Image>().sprite = imageSpanish;

            if (pnlImage != null)
                pnlImage.GetComponent<Image>().sprite = imageSpanish;
        }
    }

}
