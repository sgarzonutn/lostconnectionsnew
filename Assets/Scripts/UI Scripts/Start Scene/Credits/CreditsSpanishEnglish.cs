﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsSpanishEnglish : MonoBehaviour
{
    public Text textLanguage;
    private int valor;
    private int valorAux;
    private string texto;

    void Awake()
    {
        texto = "";
        valorAux = -1;
    }

    void Update()
    {
        valor = PlayerPrefs.GetInt("LenguajeGuardado", 0);        
        
        if(valor != valorAux)
        {
            valorAux = valor;

            if (valor == 0)
                texto = "* Andrea Massimino (Game Designer & Artist) \n\n" +
                        "* Daniela Rosales (Producer & Artist)  \n\n" +
                        "* Laura Velazquez (Programmer) \n\n" +
                        "* Sergio Garzón (Programmer)";
            else
                texto = "* Andrea Massimino (Game Designer - Artista) \n\n" +
                        "* Daniela Rosales (Productora - Artista) \n\n" +
                        "* Laura Velazquez (Programadora) \n\n" +
                        "* Sergio Garzón (Programador)";


            this.textLanguage.text = texto;
        }

        
    }



}
