﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPanel : MonoBehaviour
{
    public GameObject panel1;
    public GameObject panel2;

    public void OpenPanelVirus()
    {
        if(panel1 != null)
            this.panel1.SetActive(false);

        if(panel2 != null)
            this.panel2.SetActive(true);

    }

    public void ValidateDataPanel()
    {

        switch(panel1.name)
        {
            case "PnlTarjetaGB":
                if (panel1.name.Equals("PnlTarjetaGB"))
                {
                    MovementPlayerNewWorld mov = GameObject.Find("ObjectsWorldScene/ObjectPlayers").transform.GetChild(0).GetComponent<MovementPlayerNewWorld>();
                    mov.SetMovementPlayer(false);

                    CameraPlayer cam = Camera.main.GetComponent<CameraPlayer>();
                    cam.setCameraMovement(false);

                }
                break;
        }
        
    }

    public void ReactivateCameraPlayerMov()
    {
        MovementPlayerNewWorld mov = GameObject.Find("ObjectsWorldScene/ObjectPlayers").transform.GetChild(0).GetComponent<MovementPlayerNewWorld>();
        mov.SetMovementPlayer(true);
        
        CameraPlayer cam = Camera.main.GetComponent<CameraPlayer>();
        cam.setCameraMovement(true);
    }
}
